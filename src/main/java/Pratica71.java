import java.util.ArrayList;
import java.util.List;

import java.util.ListIterator;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.Time;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;


public class Pratica71
{
  public static void main(String[] args)
  {
      
    Time time1 = new Time();

    Scanner scanner = new Scanner(System.in);
    int numeroJogadores;
    int i;
    String posicaoJogador = new String();
    String nomeJogador = new String();
    int numeroJogador;
    int check = 1;
    
    System.out.println("Digite o número de jogadores: ");
    numeroJogadores = scanner.nextInt();
     
    for(i = 0; i<numeroJogadores; i++){
        System.out.print("Nome: ");
        nomeJogador = scanner.next();
        
        System.out.print("Posição: ");
        posicaoJogador = scanner.next();
        
        System.out.print("Numero:");
        numeroJogador = scanner.nextInt();
        
        time1.addJogador(posicaoJogador, new Jogador(numeroJogador, nomeJogador));
    }
    
    List<Jogador> jogadores_1 = new ArrayList<>();
    jogadores_1 = time1.ordena(new JogadorComparator(true, true, false));
    
    ListIterator<Jogador> it = jogadores_1.listIterator();
        
        while(it.hasNext()){
            System.out.println(it.next().numero + " ");
            it.previous();
            System.out.println(it.next().nome);
        }
    
    while(check == 1){
        
        System.out.print("Numero:");
        numeroJogador = scanner.nextInt();
        if(numeroJogador == 0){
            check = 0;
            break;
        }
        
        System.out.print("Nome: ");
        nomeJogador = scanner.next();
        
        System.out.print("Posição: ");
        posicaoJogador = scanner.next();        
        
        Jogador insere = new Jogador(numeroJogador, nomeJogador);
                              
        jogadores_1 = time1.ordena(new JogadorComparator(true, true, false));
        
        it = jogadores_1.listIterator();
        
        while(it.hasNext()){
            System.out.println(it.next().numero + " ");
            it.previous();
            System.out.println(it.next().nome);
        }
    }
  }  
}